import React, { Component } from 'react'
import { StyleSheet, Text, View, TextInput, Alert, Image,ScrollView,TouchableOpacity } from 'react-native';
import { Button, InputItem, List, WhiteSpace, WingBlank ,Icon} from '@ant-design/react-native'
import {connect} from 'react-redux'
import axios from 'axios'

class Register extends Component {
    state={
        email:'',
        password:'',
        confirmPassword:'',
        firstname:'',
        lastname:''
    }

    goLogin=()=>{
        this.props.history.push('/')
    }

    register=()=>{

        if(this.state.email === ''){
            Alert.alert('please input your email')
        }
        if(this.state.password===''){
            Alert.alert('please input password')
        }
        if(this.state.firstname === ''){
            Alert.alert('please input your firstname')
        }
        if(this.state.lastname === ''){
            Alert.alert('please input your lastname')
        }
        if(this.state.password !== this.state.confirmPassword){
            Alert.alert('password not macth')
        }else{
            axios.post('https://zenon.onthewifi.com/ticGo/users/register',{
                email: this.state.email,
                password: this.state.password,
                firstName:this.state.firstname,
                lastName:this.state.lastname
                })
                .then(response=>{
                    this.props.history.push('/')
                    alert('registeration complete')
                })
    
                .catch(error=>{
                    console.log(error.response)
                    // if(error.response.data.errors.email === ''){
                    //     alert('Please input your email')
                    // }
                    // if(error.response.data.errors.password === ''){
                    //     alert('Please input your password')
                    // }
                    // if(error.response.data.errors.firstName === ''){
                    //     alert('Please input your firstname')
                    // }
                    // if(error.response.data.errors.lastName === "Last name can't be blank"){
                    //     alert('Please input your lastname')
                    // }
                })
        }

      
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#A9A9A9' }}>
                <View style={{ flexDirection: 'row', backgroundColor: 'black' }}>
                  <TouchableOpacity style={{borderRigthColor:'white'}} onPress={()=>this.goLogin()}>
                        <Icon name="left"/>
                  </TouchableOpacity>
                </View>
                
                <ScrollView style={{ backgroundColor: '#A9A9A9', flex: 1 ,marginTop:50}}>
                    
                        <View style={{backgroundColor:'#A9A9A9'}}>
                            <Text style={{ fontWeight: '500',color:'black' }}>Email</Text>
                            <InputItem  onChangeText={(email) => this.setState({ email })}></InputItem>
                            <Text style={{ fontWeight: '500',color:'black' }}>Password</Text>
                            <InputItem type="password" onChangeText={(password) => this.setState({ password })}></InputItem>
                            <Text style={{ fontWeight: '500',color:'black' }}>Confirm Password</Text>
                            <InputItem type="password" onChangeText={(confirmPassword) => this.setState({ confirmPassword })}></InputItem>
                            <Text style={{ fontWeight: '500',color:'black' }}>Firstname</Text>
                            <InputItem onChangeText={(firstname) => this.setState({ firstname })}></InputItem>
                            <Text style={{ fontWeight: '500',color:'black' }}>Lastname</Text>
                            <InputItem onChangeText={(lastname) => this.setState({ lastname })}></InputItem>
                        </View>
                 
                    <View>
                         <Button style={{backgroundColor:'black',color:'white', borderColor: 'black',margin:10}} type="primary" onPress={this.register}>Register</Button>
                    </View>

                </ScrollView>
               

               

            </View>
        )
    }

}
const styles = StyleSheet.create({
    center: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop:10
    }
})

const mapDispatchToProps = (dispatch) =>{
    return{
        register :(username,password,firstname,lastname)=>{
            dispatch({
                type: 'ADD_ACCOUNT',
                username: username,
                password:password,
                firstname:firstname,
                lastname:lastname

            })
        }
    }
}

export default connect (null,mapDispatchToProps)(Register)