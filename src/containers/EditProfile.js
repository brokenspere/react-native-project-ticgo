import React, { Component } from 'react'
import { Text, View, Alert, ScrollView, TouchableOpacity, Image } from 'react-native';
import { Icon, TabBar, WhiteSpace, WingBlank, List, InputItem, Button } from '@ant-design/react-native';
import axios from 'axios'
import { connect } from 'react-redux'

class EditProfile extends Component {

    state = {

        firstName: '',
        lastName: ''
    }

    showListMovie = () => {
        this.props.history.push('/main')
        // this.renderItem()
    }

    showHistory = () => {
        this.props.history.push('/history')
    }

    showProfile = () => {
        this.props.history.push('/profile')
    }

    edit = () => {
        console.log('edit function')
        // axios.put('https://zenon.onthewifi.com/ticGo/users',{
        //    firstname: this.state.firstname,
        //    lastname: this.state.lastname,
        //    headers:{
        //          'Authorization':   `Bearer ${this.props.user.token}` 
        //    }

        // })
        // .then(response=>{
        //     console.log(response)

        //      this.props.saveUser(response.data.user.firstName,response.data.user.lastName)
        //      Alert.alert('success')
        // .catch(error=>{
        //     console.log(error.response)
        //     Alert.alert('success')
        // })


        // })\
        console.log(this.props.user.token)
        axios({

            url: 'https://zenon.onthewifi.com/ticGo/users',
            method: 'put',
            headers: {
                'Authorization': `Bearer ${this.props.user.token}`
                //'Authorization': "bearer " + this.props.user.token
                //    `Bearer ${this.props.user.token}`
            },
            data: {
                firstName: this.state.firstName,
                lastName: this.state.lastName
            }



        }).then(() => {
            return axios.get('https://zenon.onthewifi.com/ticGo/users', {
                headers: {
                    'Authorization': `Bearer ${this.props.user.token}`
                    //'Authorization': "bearer " + this.props.user.token
                    //    `Bearer ${this.props.user.token}`
                },
                

            }
            )


        })
            .then(response => {
                console.log(response)

                this.props.saveUser(response.data.user);
                // ({ firstName, lastName }) 
                this.props.history.push('/main')
            }).catch(e => {
                console.log("error " + e)
                // alert(e.response.data.errors.email)
            })

    }


    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'black' }} >
                <View style={{ flexDirection: 'row', backgroundColor: 'black', alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ fontWeight: '500', color: '#FF6347' }}>edit</Text>
                </View>
               

                <ScrollView style={{ flex: 1, backgroundColor: '#A9A9A9' }}>
                    <WhiteSpace />
                    <WingBlank>
                    
                        <View>

                            <Text style={{ fontWeight: '500', color: 'black' }}>Firstname</Text>
                            <InputItem onChangeText={(firstName) => this.setState({ firstName })}></InputItem>
                            <Text style={{ fontWeight: '500', color: 'black' }}>Lastname</Text>
                            <InputItem onChangeText={(lastName) => this.setState({ lastName })}></InputItem>
                        </View>
                    </WingBlank>
                    <View>
                        <Button style={{ backgroundColor: 'black', color: 'white', borderColor: 'black', margin: 10 }} type="primary" onPress={() => this.edit()}>edit</Button>
                    </View>
                </ScrollView>


                <View style={{ flexDirection: 'row', backgroundColor: 'black', justifyContent: 'space-evenly' }}>
                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.showListMovie}>
                        <Icon name="desktop" />
                        <Text style={{ color: '#FF6347' }}>now showing</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.showHistory}>
                        <Icon name="ordered-list" />
                        <Text style={{ fontWeight: '100', color: '#FF6347' }}>history</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.showProfile}>
                        <Icon name="user" />
                        <Text style={{ fontWeight: '100', color: '#FF6347' }}>profile</Text>
                    </TouchableOpacity>

                </View>


            </View >
        )
    }
}

const mapStatetoProps = (state) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        saveUser: ({ firstName, lastName }) => {
            dispatch({
                type: 'EDIT_USER',
                firstName: firstName,
                lastName: lastName

            })
        }
    }




}
export default connect(mapStatetoProps, mapDispatchToProps)(EditProfile)