import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, ImageBackground, TouchableOpacity, ScrollView, FlatList, Dimensions } from 'react-native';
import { TabBar, Icon, ActivityIndicator, Button, Tabs, Card } from '@ant-design/react-native'
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import axios from 'axios'; 

var options = { year: 'numeric', month: 'short', day: 'numeric' }

UNSAFE_componentWillMount = () => {
    const item = this.props.location.state.item.item
    console.log(item)
   this.setState({ movie: this.props.location.state.item.item })
   this.renderSeat();
    this.setState({ seats: item.seats })
    

}


class BookMovie extends Component {
    state = {
        movie: [],
        seats: [],
        booking: [],
        check: true,
        total: [],
        new: []
    }

    UNSAFE_componentWillMount() {
        const item = this.props.location.state.item.item
        this.setState({ seats: item.seats })
        this.setState({ movie: this.props.location.state.item.item })
        this.renderSeat();
        console.log('type seat ', this.state.seats)
    }
    
    renderImage = () => {
        var imgSource = this.state.check ? seatSofa : seatSofa_check
        return (
            <View>
                <Image
                    source={imgSource}
                />
            </View>
        )
    }


    renderOrderSeat = (item) => {
        if (item.item.type === "SOFA") {
            return (
                <View style={{ flex: 1, backgroundColor: '#29363C', alignItems: 'center', justifyContent: 'center', flexDirection: 'row', }}>
                    <View style={{ flexDirection: 'row', margin: 5, alignItems: 'center', justifyContent: 'center' }}>
                        <View>
                            <View style={{ alignItems: 'center', justifyContent: 'center', marginLeft: 10, marginRight: 10 }}>
                                <Image style={{ width: 40, height: 15 }} source={require('../images/seat-3.png')} />
                                <Text style={{ color: 'white', fontSize: 8 }}>{item.item.name}</Text>
                                <Text style={{ color: 'white', fontSize: 8 }}>{item.item.price} THB</Text>
                            </View>
                        </View>
                    </View>
                </View>
            )
        } else if (item.item.type === "PREMIUM") {
            return (
                <View style={{ flex: 1, backgroundColor: '#29363C', alignItems: 'center', justifyContent: 'center', flexDirection: 'row', }}>
                    <View style={{ flexDirection: 'row', margin: 5, alignItems: 'center', justifyContent: 'center' }}>
                        <View>
                            <View style={{ alignItems: 'center', justifyContent: 'center', marginLeft: 10, marginRight: 10 }}>
                                <Image style={{ width: 15, height: 15 }} source={require('../images/seat-2.png')} />
                                <Text style={{ color: 'white', fontSize: 8 }}>{item.item.name}</Text>
                                <Text style={{ color: 'white', fontSize: 8 }}>{item.item.price} THB</Text>
                            </View>
                        </View>
                    </View>
                </View>
            )
        } else {
            return (
                <View style={{ flex: 1, backgroundColor: '#29363C', alignItems: 'center', justifyContent: 'center', flexDirection: 'row', }}>
                    <View style={{ flexDirection: 'row', margin: 5, alignItems: 'center', justifyContent: 'center' }}>
                        <View>
                            <View style={{ alignItems: 'center', justifyContent: 'center', marginLeft: 10, marginRight: 10 }}>
                                <Image style={{ width: 15, height: 15 }} source={require('../images/seat-1.png')} />
                                <Text style={{ color: 'white', fontSize: 8 }}>{item.item.name}</Text>
                                <Text style={{ color: 'white', fontSize: 8 }}>{item.item.price} THB</Text>
                            </View>
                        </View>
                    </View>
                </View>
            )
        }
    }

    checkSeat = (item) => {
        if (item === false || this.state.seat === false) {
            this.setState({ seat: !this.state.seat });
            return (
                <Image style={{ width: 25, height: 8, }} source={require('../images/seat-3.png')} />
            )
        } else if (item === true) {
            return (
                <Image style={{ width: 25, height: 8, }} source={require('../images/user.png')} />
            )
        } else if (this.state.seat === true) {
            this.setState({ seat: !this.state.seat });
            return (
                <Image style={{ width: 25, height: 8, }} source={require('../images/seat-3.png')} />
            )
        }
        else {
            this.setState({ seat: !this.state.seat });
            return (
                <Image style={{ width: 25, height: 8, }} source={require('../images/correctSign.png')} />
            )
        }
    }
    showListMovie = () => {
        this.props.history.push('/main')

    }

    showHistory = () => {
        this.props.history.push('/history')
    }

    showProfile = () => {
        this.props.history.push('/profile')
    }

    goSummary = () => {
        this.props.history.push('summary')
    }

    onPressSeat = (type, row, column, price) => {
        this.state.booking.push({ column: (column+1), row: (row+1), type: type })
        let item = this.state.new
        item = item.map(i => {
            if (i.type !== type) {
                return i
            }
            return Object.keys(i).reduce((sum, each, indexRows) => {
                sum[each] = i[each]
                if (each.includes('row') && !each.includes('rows')) {
                    sum[each] = sum[each].map((item, indexCulumn) => {
                        if (each === `row${(row + 1)}` && indexCulumn === column && item === 'F') {
                            this.state.total.push(price)
                            return 'C'
                        }
                        else {
                            if (each === `row${(row + 1)}` && indexCulumn === column && item === 'C') {

                                return 'F'
                            } else {
                                return item
                            }
                        }
                    })
                    
                }
               
                return sum
            }, {})
        })
        console.log('POOM2: ', item);
        this.setState({ new: item })

       
        console.log('Booking:', this.state.booking)
    }

    renderSeat = () => {
        let item = this.props.location.state.item.item.seats
        item = item.map(i => {
            return Object.keys(i).reduce((sum, each) => {
                sum[each] = i[each]
                if (each.includes('row') && !each.includes('rows')) {
                    sum[each] = sum[each].map(item => item === true ? 'B' : 'F')
                    console.log();
                }
                return sum
            }, {})
        })
        console.log( item);
        this.setState({ new: item })
    }

    Booking = () => {
        const { user } = this.props
        axios({
            method: 'post',
            url: 'https://zenon.onthewifi.com/ticGo/movies/book',
            headers:{'Authorization': `Bearer ${this.props.user.token}`},
            data: {
                showtimeId: this.state.movie._id,
                seats: this.state.booking
            }
        })
            .then(() => {
                alert('Book success')
                this.props.history.push('/main')
            })
            .catch((error) => {
                
                alert('you cannot book this time')
            })
    }

    _keyExtractorSeats = (item, index) => item._id

    _keyExtractorDetailSeat = (item, index) => item._id

    render() {
        console.log("SUSk", this.state.movie)
        const item = this.props.location.state.item.item
        console.log('itemM : ', item)
        const movie = this.props.location.state.item.item.movie
        var date = new Date(item.startDateTime)
        timeStart = date.toLocaleTimeString().replace(/(.*)\D\d+/, '$1')
        var date2 = new Date(item.endDateTime)
        timeEnd = date2.toLocaleTimeString().replace(/(.*)\D\d+/, '$1')
        date = date.toLocaleDateString("th-TH", options)
        var cinema = this.props.location.state.item.item.cinema.name
        console.log('Test222: ', this.state.new)
        console.log('Booking,:', this.state.booking)
        return (

            <View style={{ flex: 1, backgroundColor: 'black', }}>
                <View style={{ flexDirection: 'column', justifyContent: 'center', margin: 10 }}>
                    <View style={{ flexDirection: 'row', alignItems: 'flex-start', }}>
                        <Image
                            source={{ uri: movie.image }}
                            style={{ width: '40%', height: 100, resizeMode: 'contain' }}
                        />
                        <View style={{ margin: 4, marginLeft: 10 }}>
                            <Text style={{ color: 'white', margin: 5 }}>{movie.name}</Text>
                            <Text style={{ color: 'white', margin: 5 }}>{date}</Text>
                            <Text style={{ color: 'white', margin: 5 }}><Icon name="clock-circle" size="xs" /> {timeStart}</Text>
                            <Text style={{ color: 'white', margin: 5 }}>{cinema} | &nbsp;<Icon name="sound" size="xxs" color="white" /> {item.soundtrack}</Text>
                        </View>
                    </View>
                </View>
                <FlatList
                    keyExtractor={this._keyExtractorSeats}
                    numColumns={item.seats.length}
                    data={item.seats}
                    renderItem={(item) => (
                        this.renderOrderSeat(item)
                    )}
                />
               <ScrollView>
                    <View style={styles.content}>
                        <Image style={styles.screen} source={require('../images/screen.png')} />
                        {
                            this.state.new.map((i, index) => {
                                console.log('i: ', i)
                                return Object.keys(i).map((rowsData, indexRows) => {
                                   
                                    console.log('rowsData: ', rowsData)
                                   
                                    return (
                                        <FlatList
                                            keyExtractor={this._keyExtractorSeats}
                                            data={i['row' + (indexRows + 1)]}
                                            numColumns={i.columns}
                                            extraData={this.state}
                                            renderItem={({ item, index }) => {
                                                console.log('eieiei', i.type)
                                               

                                                if (item === 'F') {
                                                    if (i.type === "SOFA") {
                                                        return (
                                                            <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, i.price) }}>
                                                                <Image style={styles.seat3B} source={require('../images/seat-3.png')} />
                                                            </TouchableOpacity>
                                                        )
                                                    }
                                                    else if (i.type === "PREMIUM") {
                                                        return (
                                                            <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, i.price) }}>
                                                                <Image style={styles.seat1B} source={require('../images/seat-2.png')} />
                                                            </TouchableOpacity>
                                                        )
                                                    }
                                                    else if (i.type === "DELUXE") {
                                                        return (
                                                            <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, i.price) }}>
                                                                <Image style={styles.seat1B} source={require('../images/seat-1.png')} />
                                                            </TouchableOpacity>
                                                        )
                                                    }
                                                }
                                                else if (item === 'C') {
                                                    return (
                                                        <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, i.price) }}>
                                                            <Image style={styles.seat1B} source={require('../images/correctSign.png')} />
                                                        </TouchableOpacity>
                                                    )
                                                }
                                                else if (item === 'B') {
                                                    return <Image style={styles.seat1B} source={require('../images/user.png')} />
                                                }
                                            }
                                            }
                                        />
                                    )
                                })
                            }).reverse()
                        }
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#29363C' }}>
                        <View style={{ flexDirection: 'row' ,margin:2}}>
                            <Text style={{ color: 'black', fontSize: 14, justifyContent: 'flex-end' }}>Total: {this.state.total.reduce((x, y) => { return x + y }, 0)} Bath</Text>
                            <View style={{margin:2}}>
                            <Button type='primary' size="small" onPress={() => { this.Booking() }}>Book now</Button>
                        </View>
                        </View>
                        
                    </View>
                </ScrollView>
                <View style={{ flexDirection: 'row', backgroundColor: 'black', justifyContent: 'space-evenly'}}>
                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.showListMovie}>
                        <Icon name="desktop" />
                        <Text style={{ color: '#FF6347' }}>now showing</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.showHistory}>
                        <Icon name="ordered-list" />
                        <Text style={{ fontWeight: '100', color: '#FF6347' }}>history</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.showProfile}>
                        <Icon name="user" />
                        <Text style={{ fontWeight: '100', color: '#FF6347' }}>profile</Text>
                    </TouchableOpacity>

                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#455a64',
    },
    boxHeader: {
        backgroundColor: '#d1dae0',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    back: {
        padding: 10,
        margin: 2,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        flex: 5,
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    HeaderContent: {
        flexDirection: 'column',
        justifyContent: 'center',
        margin: 10
    },
    detailMovie: {
        flexDirection: 'row',
        alignItems: 'flex-start',
    },
    detailSeat: {
        flex: 1,
        backgroundColor: '#34444b',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    content: {
        flex: 1,
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    infoMovie: {
        margin: 5,
        marginLeft: 10
    },
    seat1: {
        width: 15,
        height: 15
    },
    seat3: {
        width: 40,
        height: 15
    },
    seat1B: {
        width: 10,
        height: 10,
        margin: 2
    },
    seat3B: {
        width: 25,
        height: 8,
    },
    user: {
        width: 8,
        height: 8,
    },
    textDetailSeat: {
        color: 'white',
        fontSize: 8
    },
    detail: {
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    screen: {
        width: 300,
        height: 35,
       
    }
});

const mapStatetoProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStatetoProps, null)(BookMovie)