import React, { Component } from 'react'
import { Text, View, Alert, ScrollView, TouchableOpacity, Image } from 'react-native';
import { Icon, TabBar, Button, WhiteSpace, WingBlank } from '@ant-design/react-native';
import { connect } from 'react-redux'
import ImagePicker from 'react-native-image-picker'
import axios from 'axios'



class Profile extends Component {

    showListMovie = () => {
        this.props.history.push('/main')
        // this.renderItem()
    }

    showHistory = () => {
        this.props.history.push('/history')
    }

    showProfile = () => {
        this.props.history.push('/profile')
    }

    goEditprofile = () => {
        this.props.history.push('/editprofile')
    }
   
    logOut =()=>{
        this.props.history.push('/')
    }

    


    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'black' }}>
                {/* <View style={{ flexDirection: 'row', backgroundColor: 'black', alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ fontWeight: '500', color: '#FF6347' }}>Profile</Text>
                </View> */}



                <View style={{ flex: 1, backgroundColor: '#A9A9A9', alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>

                    <WhiteSpace />

                    <WingBlank>
                        <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#A9A9A9',margin:4 }}>
                            <Image style={{ width: 100, height: 100, borderRadius: 100 }} source={{ uri: 'https://png.pngtree.com/svg/20170602/person_1058425.png' }} />
                        </View>
                        <View style={{ flexDirection: 'row' }}>

                            <Text style={{ fontWeight: 'bold', color: 'black', fontSize: 20 }}>Firstname:</Text>
                            <Text style={{ fontSize: 20 }}>{this.props.user.firstName}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>

                            <Text style={{ fontWeight: 'bold', color: 'black', fontSize: 20 }}>Lastname:</Text>
                            <Text style={{ fontSize: 20 }}>{this.props.user.lastName}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ fontWeight: 'bold', color: 'black', fontSize: 20 }}>Email:</Text>
                            <Text style={{ fontSize: 20 }}>{this.props.user.email}</Text>
                        </View>
                        <Button style={{ backgroundColor: 'black', color: 'white', borderColor: 'black', margin: 10}} type="primary" onPress={() => this.goEditprofile()}>edit</Button>
                        <Button style={{ margin: 10}} type="warning" size="small" onPress={()=>this.logOut()}>logout</Button>
                    </WingBlank>
                </View>

                <View style={{ flexDirection: 'row', backgroundColor: 'black', justifyContent: 'space-evenly' }}>
                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.showListMovie}>
                        <Icon name="desktop" />
                        <Text style={{ color: '#FF6347' }}>now showing</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.showHistory}>
                        <Icon name="ordered-list" />
                        <Text style={{ fontWeight: '100', color: '#FF6347' }}>history</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.showProfile}>
                        <Icon name="user" />
                        <Text style={{ fontWeight: '100', color: '#FF6347' }}>profile</Text>
                    </TouchableOpacity>

                </View>


            </View>
        )
    }
}

const mapStatetoProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStatetoProps, null)(Profile)