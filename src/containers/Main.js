import React, { Component } from 'react'
import { Text, View, Alert, ScrollView, TouchableOpacity, Image, FlatList } from 'react-native';
import { Icon, TabBar, Grid } from '@ant-design/react-native';
import axios from 'axios'
import Item from '@ant-design/react-native/lib/list/ListItem';
import { push } from 'connected-react-router'
import { connect } from 'react-redux'


class Main extends Component {
    state = {
        products: []

    }
   
    renderItem=()=>{
        axios.get('https://zenon.onthewifi.com/ticGo/movies')
        .then(response => response.data)
        .then(data => {
            this.setState({ products: data })
            console.log('movies', this.state.products)
        })
    }

     componentDidMount = () => {
       this.renderItem()

    }

    showListMovie = () => {
        this.props.history.push('/main')
        // this.renderItem()
    }

    showHistory = () => {
        this.props.history.push('/history')
    }

    showProfile = () => {
        this.props.history.push('/profile')
    }

    onClickMovie = (item)=>{
        this.props.history.push('/detail', {
            movieDetail: item
        })
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'black' }}>
                <View style={{ flexDirection: 'row', backgroundColor: 'black', alignItems: 'center', justifyContent: 'center' ,height:40}}>
                    <Text style={{ fontWeight: '500', color: '#FF6347' }}>Now showing</Text>
                </View>

                <ScrollView style={{ flex: 1, backgroundColor: 'white' }} >

                    <Grid

                        columnNum={2}
                        data={this.state.products}
                        itemStyle={{ height: 272, margin: 10 }}
                        renderItem={(item) => (
                            <TouchableOpacity 
                                onPress={()=>this.onClickMovie(item)}
                            >
                                <Image style={{ width: '100%', height: 250 }} source={{ uri: item.image }} />
                               <View style={{backgroundColor:'gray'}}>
                               <Text style={{color:'white',textAlign:'center'}}>{item.name}</Text>
                               </View>
                               
                            </TouchableOpacity>
                        )}
                    />

                </ScrollView>


                <View style={{ flexDirection: 'row', backgroundColor: 'black' , justifyContent:'space-evenly'}}>
                    <TouchableOpacity style={{alignItems:'center'}} onPress={this.showListMovie}>
                        <Icon name="desktop" />
                        <Text style={{ color: '#FF6347' }}>now showing</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{alignItems:'center'}} onPress={this.showHistory}>
                        <Icon name="ordered-list" />
                        <Text style={{ fontWeight: '100', color: '#FF6347' }}>history</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{alignItems:'center'}} onPress={this.showProfile}>
                        <Icon name="user" />
                        <Text style={{ fontWeight: '100', color: '#FF6347' }}>profile</Text>
                    </TouchableOpacity>

                </View>


            </View>
        )
    }
}

export default connect(state => state, { push })(Main)