import React, { Component } from 'react'
import { Text, View, Alert, ScrollView, TouchableOpacity, Image } from 'react-native';
import { Icon, TabBar, Button, WhiteSpace, WingBlank } from '@ant-design/react-native';
import { connect } from 'react-redux'
import ImagePicker from 'react-native-image-picker'
import axios from 'axios'



class Profile extends Component {

    state={
        imagePath: 'https://png.pngtree.com/svg/20170602/person_1058425.png'
    }


    showListMovie = () => {
        this.props.history.push('/main')
        // this.renderItem()
    }

    showHistory = () => {
        this.props.history.push('/history')
    }

    showProfile = () => {
        this.props.history.push('/profile')
    }

    goEditprofile = () => {
        this.props.history.push('/editprofile')
    }
   
    logOut =()=>{
        this.props.history.push('/')
       this.props.delete()
    }

    onPressImage = () => {
        const { user } = this.props
        ImagePicker.showImagePicker({}, (response) => {
            console.log(response)
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else if (response.uri) {
                const formData = new FormData()
                formData.append('image', {
                    uri: response.uri,
                    name: response.fileName,
                    type: response.type,
                })
                axios.post('https://zenon.onthewifi.com/ticGo/users/image', formData, {
                    headers: {
                        Authorization: `Bearer ${user.token}`
                    },
                    onUploadProgress: progressEvent => {
                        console.log('progress', Math.floor(progressEvent.loaded / progressEvent.total * 100))
                    }
                })
                    .then(response => {
                        this.setState({
                            imagePath: response.data.image
                        })
                    })
                    .catch(error => {
                        console.log(error.response)
                    })
                // this.setState({
                //     imageURI: response.uri
                // })
            }
        })
    }

    

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'black' }}>
                {/* <View style={{ flexDirection: 'row', backgroundColor: 'black', alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ fontWeight: '500', color: '#FF6347' }}>Profile</Text>
                </View> */}



                <View style={{ flex: 1, backgroundColor: '#A9A9A9', alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>

                    <WhiteSpace />

                    <WingBlank>
                        <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#A9A9A9',margin:4 }} >
                            <TouchableOpacity onPress={() => this.onPressImage()}>
                                 <Image style={{ width: 100, height: 100, borderRadius: 100 }} source={{ uri: this.state.imagePath }} />
                            </TouchableOpacity>
                            
                        </View>
                        <View style={{ flexDirection: 'row' }}>

                            <Text style={{ fontWeight: 'bold', color: 'black', fontSize: 20 }}>Firstname:</Text>
                            <Text style={{ fontSize: 20 }}>{this.props.user.firstName}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>

                            <Text style={{ fontWeight: 'bold', color: 'black', fontSize: 20 }}>Lastname:</Text>
                            <Text style={{ fontSize: 20 }}>{this.props.user.lastName}</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ fontWeight: 'bold', color: 'black', fontSize: 20 }}>Email:</Text>
                            <Text style={{ fontSize: 20 }}>{this.props.user.email}</Text>
                        </View>
                        <Button style={{ backgroundColor: 'black', color: 'white', borderColor: 'black', margin: 10}} type="primary" onPress={() => this.goEditprofile()}>edit</Button>
                        <Button style={{ margin: 10}} type="warning" size="small" onPress={()=>this.logOut()}>logout</Button>
                    </WingBlank>
                </View>

                <View style={{ flexDirection: 'row', backgroundColor: 'black', justifyContent: 'space-evenly' }}>
                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.showListMovie}>
                        <Icon name="desktop" />
                        <Text style={{ color: '#FF6347' }}>now showing</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.showHistory}>
                        <Icon name="ordered-list" />
                        <Text style={{ fontWeight: '100', color: '#FF6347' }}>history</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.showProfile}>
                        <Icon name="user" />
                        <Text style={{ fontWeight: '100', color: '#FF6347' }}>profile</Text>
                    </TouchableOpacity>

                </View>


            </View>
        )
    }
}



const mapStatetoProps = (state) => {
    return {
        user: state.user
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        delete: () => {
            dispatch({
                type: 'DELETE_USER',
               
            })
        }
    }
}

export default connect(mapStatetoProps, mapDispatchToProps)(Profile)