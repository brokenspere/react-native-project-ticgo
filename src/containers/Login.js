import React, { Component } from 'react'
import { Text, View, Alert, ScrollView, TouchableOpacity,Image } from 'react-native';
import { Button, InputItem, WhiteSpace, WingBlank, List,Icon } from '@ant-design/react-native'
import { connect } from 'react-redux'
import axios from 'axios'

class Login extends Component {
    state = {
        email: '',
        password: ''
    }

    login = () => {
        
            axios.post('https://zenon.onthewifi.com/ticGo/users/login',{
                email: this.state.email,
                password: this.state.password
                
                
            })
                .then(response=>{
                    console.log(response)
                     this.props.history.push('/main')
                    this.props.saveUser(response.data.user)
                    console.log('user',this.props)
                })
                .catch(error=>{
                    console.log(error.response)
                    if(error.response.data.errors.email === 'is invalid'){
                        alert('email is invalid')
                    }
                    if(error.response.data.errors.password === 'is invalid'){
                        alert('password is invalid')
                    }
                })
                
        
      
          
    }
    goRegister=()=>{
        this.props.history.push('/register')
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
               
                <View style={{backgroundColor:'#A9A9A9'}}>
                <View style={{ margin:40 , alignItems: 'center', justifyContent: 'center' }}>
                <Image style={{width:100,height:100}} source={{ uri: 'https://png.pngtree.com/svg/20160325/clapperboard_959584.png' }} />
                </View>
                </View>
               
                <ScrollView style={{ backgroundColor: '#A9A9A9', flex: 1 }}>
                    
                        <View style={{backgroundColor:'#A9A9A9'}}>
                            <Text style={{ fontWeight: '500', color: 'black' ,margin:10 }}>Email</Text>
                            <InputItem  onChangeText={(email) => this.setState({ email })}></InputItem>
                            <Text  style={{ fontWeight: '500', color: 'black',margin:10 }}>Password</Text>
                            <InputItem  type="password" onChangeText={(password) => this.setState({ password })}></InputItem>
                        </View>
                   
                    <View>
                        <Button style={{ backgroundColor: 'black', color: '#FF6347', borderColor: 'black', margin: 10 }} type="primary" onPress={()=>this.login()}>Login</Button>
                    </View>
                    <View style={{alignItems:'center', justifyContent: 'center'}}>
                        <Text style={{margin:6}}>OR</Text>
                        <TouchableOpacity onPress={this.goRegister}>
                            <Text style={{fontStyle:'italic',fontWeight:'bold',color:'black'}}>Register here</Text>
                        </TouchableOpacity>
                    </View>

                </ScrollView>
               



            </View>

        )
    }

}

const mapDispatchToProps =(dispatch) =>{
    return {
        saveUser: ({email,password,firstName,lastName,token})=>{
            dispatch({
                type:'ADD_ACCOUNT',
                email: email,
                password:password,
                firstName:firstName,
                lastName:lastName,
                token: token
            })
        }
    }

}


export default connect(null, mapDispatchToProps)(Login)