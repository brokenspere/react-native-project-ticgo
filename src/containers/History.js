import React, { Component } from 'react'
import { Text, View, Alert, ScrollView, TouchableOpacity, Image, FlatList } from 'react-native';
import { Icon, TabBar, Card, Button } from '@ant-design/react-native';
import { connect } from 'react-redux'
import axios from 'axios'


class History extends Component {

    state = {
        history: [],
        movieNames: []
    }

    showListMovie = () => {
        this.props.history.push('/main')
        // this.renderItem()
    }

    showHistory = () => {
        this.props.history.push('/history')
    }

    showProfile = () => {
        this.props.history.push('/profile')
    }

    componentDidMount = () => {
        axios({
            url: 'https://zenon.onthewifi.com/ticGo/movies/book',
            method: 'get',
            headers: {
                'Authorization': `Bearer ${this.props.user.token}`
            },
        }).then(response => response.data)
            .then(data => {
                this.setState({ history: data })

                console.log('history', this.state.history)
                return data
            })
            .then(data => {
                data.forEach((element, index) => {
                    console.log('aa', element.showtime)
                    axios.get('https://zenon.onthewifi.com/ticGo/movies/showtimes/' + element.showtime)
                        .then(response => response.data)
                        .then(data => {
                            this.setState({
                                movieNames: [
                                    ...this.state.movieNames,
                                    data


                                ]



                            })
                            console.log('name', this.state.movieNames)
                            console.log(this.state.history[0])
                        })
                })
                return data
            })
    }



    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'black' }}>
                <View style={{ flexDirection: 'row', backgroundColor: 'black', alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ fontWeight: '500', color: '#FF6347' }}>History</Text>
                </View>

                <ScrollView style={{ flex: 1, backgroundColor: 'black' }}>

                    <FlatList
                        style={{ flex: 1 }}
                        data={this.state.movieNames}

                        renderItem={({ item, index }) => (

                            <Card style={{ backgroundColor: 'white', margin: 5 }}>
                                <Card.Header
                                    title={item.movie.name}
                                    extra={item.cinema.name}

                                />
                                <Card.Body style={{ flexDirection: 'row' }}>
                                    <View style={{ flexDirection: 'row', margin: 4 }}>
                                        <Image style={{ width: '50%', height: 110 }} source={{ uri: item.movie.image }} resizeMode="contain" />
                                    </View>
                                    <View style={{ margin: 4 }}>

                                        <View style={{ flexDirection: 'row' }}>
                                            <Text>{new Date(this.state.history[index].createdDateTime).toLocaleDateString()}</Text>

                                        </View>
                                         <View style={{ flexDirection: 'row' }}>
                                            <Text>Duration:</Text>
                                            <Text>{item.movie.duration}</Text>
                                            <Text>min</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text>Soundtrack:</Text>
                                            <Text>{item.soundtrack}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text>Subtitle:</Text>
                                            <Text>{item.subtitle}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Button disabled activeStyle={{ backgroundColor: '#80dfff' }}>
                                                <Text style={{ color: 'black' }}>{new Date(item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')}</Text>
                                            </Button>
                                        </View>
                                    </View>
                                </Card.Body>
                            </Card>
                        )}




                    />
                </ScrollView>

                <View style={{ flexDirection: 'row', backgroundColor: 'black', justifyContent: 'space-evenly' }}>
                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.showListMovie}>
                        <Icon name="desktop" />
                        <Text style={{ color: '#FF6347' }}>now showing</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.showHistory}>
                        <Icon name="ordered-list" />
                        <Text style={{ fontWeight: '100', color: '#FF6347' }}>history</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.showProfile}>
                        <Icon name="user" />
                        <Text style={{ fontWeight: '100', color: '#FF6347' }}>profile</Text>
                    </TouchableOpacity>

                </View>


            </View>
        )
    }
}
const mapStatetoProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStatetoProps, null)(History)