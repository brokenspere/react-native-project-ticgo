import React, { Component } from 'react'
import { Text, View, Alert, ScrollView, TouchableOpacity, Image, FlatList,ImageBackground ,StyleSheet} from 'react-native';
import { Icon, TabBar, Card, Button ,Tabs } from '@ant-design/react-native';
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
// import { Card } from 'react-native-elements'
import axios from 'axios'

var options = { weekday: 'short', month: 'short', day: 'numeric' }
var tab1 = Date.now()
var tab2 = new Date(new Date().getTime() + 24 * 60 * 60 * 1000)
tab2 = tab2.getTime()
var tab3 = new Date(new Date().getTime() + 48 * 60 * 60 * 1000)
tab3 = tab3.getTime()



class MovieDetail extends Component {

    state = {
        movie: [],
        movie2: [],
        movie3: [],
        dates: new Date(),
        tabs: [
            { title: `${new Date().toLocaleDateString("en-US", options)}` },
            { title: `${new Date(new Date().getTime() + 24 * 60 * 60 * 1000).toLocaleDateString("en-US", options)}` },
            { title: `${new Date(new Date().getTime() + 48 * 60 * 60 * 1000).toLocaleDateString("en-US", options)}` },
        ],
    }

    componentDidMount() {
    //     var date = this.state.dates.getTime();
    //     axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${this.props.location.state.movieDetail._id}`,
    //         { params: { date: date } }
    //     )
    //         .then(response => {
    //             this.setState({
    //                 movie: response.data,

    //             })
    //             console.log('Movie: ', this.state.movie)
    //             console.log('State didmount: ', this.state.dates)
    //             console.log('Date Now: ', Date.now())
    //             console.log('var Date: ', date)
    //         })
    //         .catch((error) => {
    //             console.error('Error Didmount:', error);
    //         })
    //         .finally(() => { console.log('Finally') })
    //     //  console.log(this.props)
    // }

    // renderContent = (dates) => {
    //     axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${this.props.location.state.movieDetail._id}`,
    //         { params: { date: dates } }
    //     )
    //         .then(response => {
    //             this.setState({
    //                 movie: response.data,
    //             })
    //         })
    //         .catch((error) => {
    //             console.error('Error renderContent:', error);
    //         })
    //         .finally(() => { console.log('Finally') })
        this.getMovieTab1()
        this.getMovieTab2()
        this.getMovieTab3()

    }
    getMovieTab1 = () => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${this.props.location.state.movieDetail._id}`,
            { params: { date: tab1 } }
        )
            .then(response => {
                this.setState({
                    movie1: response.data,
                    
                })
            })
            .catch((error) => {
                console.error('Error POOM:', error);
            })
            .finally(() => { console.log('Finally') })
    }

    getMovieTab2 = () => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${this.props.location.state.movieDetail._id}`,
            { params: { date: tab2 } }
        )
            .then(response => {
                this.setState({
                    movie2: response.data,
                   
                })
            })
            .catch((error) => {
                console.error('Error POOM:', error);
            })
            .finally(() => { console.log('Finally') })
    }

    getMovieTab3 = () => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${this.props.location.state.movieDetail._id}`,
            { params: { date: tab3 } }
        )
            .then(response => {
                this.setState({
                    movie3: response.data,
                   
                })
            })
            .catch((error) => {
                console.error('Error POOM:', error);
            })
            .finally(() => { console.log('Finally') })
    }



    showListMovie = () => {
        this.props.history.push('/main')

    }

    showHistory = () => {
        this.props.history.push('/history')
    }

    showProfile = () => {
        this.props.history.push('/profile')
    }

    UNSAFE_componentWillMount = () => {
        console.log(this.props)
    }
   
    goToBookPage=(item)=>{
        const { push } = this.props
        const movie = this.props.location.state.item
        push('/booking', {
            item: item, movie: movie
        })
    }



    render() {
        const movie = this.props.location.state.movieDetail
        var tabsValue = [
            {
                title: this.state.tabs[0].title
            },
            {
                title: this.state.tabs[1].title
            },
            {
                title: this.state.tabs[2].title
            },
        ];

        return (
            <View style={{ flex: 1, backgroundColor: 'black' }}>
                <ImageBackground source={{uri:movie.image}} style={{width: '100%', height: '100%'}} blurRadius={5}>
                <View style={{ flexDirection: 'row', backgroundColor: 'black', alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ fontWeight: '500', color: '#FF6347' }}>Select showtimes</Text>
                </View>
                
                <ScrollView style={{ flex: 1,  flexDirection: 'row' }} >
                  
                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <View style={{ borderColor:'black'}}>
                             <Image style={{ width: 200, height: 250}} source={{ uri: movie.image }} resizeMode="contain" /> 
                        </View>
                          <View style={{backgroundColor:'black',width:120,height:90 ,borderRadius:10,alignItems:'center',justifyContent:'center'}}>
                            <View style={{ flexDirection: 'row' }}>

                                <Text style={{ marginBottom: 10, fontWeight: 'bold', color: '#FF6347' }}><Icon name="clock-circle" size="xs" /></Text>
                                <Text style={{ marginBottom: 10, color: '#FF6347' }}>{movie.duration}</Text>
                                <Text style={{ marginBottom: 10, color: '#FF6347' }}>min</Text>

                            </View>
                            <View style={{ flexDirection: 'row' }}>

                                <Text style={{ marginBottom: 10, fontWeight: 'bold', color: '#FF6347' }}>Soundtracks:</Text>
                                <Text style={{ marginBottom: 10, color: '#FF6347' }}>{movie.soundtracks}</Text>


                            </View>
                            <View style={{ flexDirection: 'row' }}>

                                <Text style={{ marginBottom: 10, fontWeight: 'bold', color: '#FF6347' }}>Subtitles:</Text>
                                <Text style={{ marginBottom: 10, color: '#FF6347' }}>{movie.subtitles}</Text>


                            </View>
                        </View>


                    </View>

                    <View style={styles.tabstest}>
                                <Tabs tabs={tabsValue} tabBarActiveTextColor="#FF6347">
                                    <View>
                                        <Card>
                                            <Card.Header
                                                title="Show times"
                                            />
                                            <Card.Body>
                                                <FlatList
                                                    numColumns={4}
                                                    data={this.state.movie1}
                                                    renderItem={(item) => ( //toLocaleTimeString().replace(/(.*)\D\d+/, '$1')
                                                        <TouchableOpacity onPress={() => this.goToBookPage(item)} style={styles.ButtonTime}>
                                                            <Text style={{ color: '#99ccff', fontSize: 12 }}> {item.item.soundtrack}</Text>
                                                            <Button onPress={() => this.goToBookPage(item)} activeStyle={{ backgroundColor: '#80dfff' }}>
                                                                <Text style={{ color: 'black' }}>{new Date(item.item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')}</Text>
                                                            </Button>
                                                        </TouchableOpacity>
                                                    )}
                                                />
                                            </Card.Body>
                                        </Card>
                                    </View>
                                    <View>
                                        <Card>
                                            <Card.Header
                                                title="Show times"
                                            />
                                            <Card.Body>
                                                <FlatList
                                                    numColumns={4}
                                                    data={this.state.movie2}
                                                    renderItem={(item) => ( //toLocaleTimeString().replace(/(.*)\D\d+/, '$1')
                                                        <TouchableOpacity onPress={() => this.goToBookPage(item)} style={styles.ButtonTime}>
                                                            <Text style={{ color: '#99ccff', fontSize: 12 }}> {item.item.soundtrack}</Text>
                                                            <Button onPress={() => this.goToBookPage(item)} activeStyle={{ backgroundColor: '#80dfff' }}>
                                                                <Text style={{ color: 'black' }}>{new Date(item.item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')}</Text>
                                                            </Button>
                                                        </TouchableOpacity>
                                                    )}
                                                />
                                            </Card.Body>
                                        </Card>
                                    </View>
                                    <View>
                                        <Card>
                                            <Card.Header
                                                title="Show times"
                                            />
                                            <Card.Body>
                                                <FlatList
                                                    numColumns={4}
                                                    data={this.state.movie3}
                                                    renderItem={(item) => ( //toLocaleTimeString().replace(/(.*)\D\d+/, '$1')
                                                        <TouchableOpacity onPress={() => this.goToBookPage(item)} style={styles.ButtonTime}>
                                                            <Text style={{ color: '#99ccff', fontSize: 12 }}> {item.item.soundtrack}</Text>
                                                            <Button onPress={() => this.goToBookPage(item)} activeStyle={{ backgroundColor: '#80dfff' }}>
                                                                <Text style={{ color: 'black' }}>{new Date(item.item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')}</Text>
                                                            </Button>
                                                        </TouchableOpacity>
                                                    )}
                                                />
                                            </Card.Body>
                                        </Card>
                                    </View>
                                </Tabs>
                            </View>

                    {/* <View style={{marginHorizontal:20 }}>
                        <Card>
                            <Card.Header
                                title="Show times"
                            />
                            <Card.Body>
                                <FlatList
                                    numColumns={4}
                                    data={this.state.movie}
                                    renderItem={(item) => ( //toLocaleTimeString().replace(/(.*)\D\d+/, '$1')
                                        <TouchableOpacity style={{ margin: 2, alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={{ color: '#99ccff', fontSize: 12 }}> {item.item.soundtrack}</Text>
                                            <Button activeStyle={{ backgroundColor: '#80dfff' }} onPress={()=>this.goToBookPage(item)}>
                                                <Text style={{ color: 'black' }}>{new Date(item.item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')}</Text>
                                            </Button>
                                        </TouchableOpacity>
                                    )}
                                />
                            </Card.Body>
                        </Card>
                    </View> */}

                    
                </ScrollView>
                <View style={{ flexDirection: 'row', backgroundColor: 'black', justifyContent: 'space-evenly' }}>
                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.showListMovie}>
                        <Icon name="desktop" />
                        <Text style={{ color: '#FF6347' }}>now showing</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.showHistory}>
                        <Icon name="ordered-list" />
                        <Text style={{ fontWeight: '100', color: '#FF6347' }}>history</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.showProfile}>
                        <Icon name="user" />
                        <Text style={{ fontWeight: '100', color: '#FF6347' }}>profile</Text>
                    </TouchableOpacity>

                </View>
                </ImageBackground>
               
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#455a64',
    },
    boxHeader: {
        backgroundColor: '#d1dae0',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    content: {
        flexDirection: 'column',
        backgroundColor: '#ffcccc',
        justifyContent: 'center',
        margin: 10
    },
    scene: {
        flex: 1,
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        height: 200,
        width: '100%',
    },
    headerImage: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    back: {
        padding: 10,
        margin: 2,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        flex: 5,
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    center: {
        alignItems: 'center'
    },
    textHead: {
        textAlign: 'center',
        fontSize: 15,
        color: 'white',
    },
    tabs: {
        backgroundColor: "white",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
    },
    time: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 150,
        backgroundColor: '#fff',
    },
    Touch: {
        backgroundColor: "gray",
        padding: 10,
        margin: 2,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textTab: {
        color: '#a6a6a6'
    },
    tabstest: {
        flex: 2,
        flexDirection: 'row',
        height: 345,
        backgroundColor: '#e1e7ea'
    },
    ButtonTime: {
        margin: 2,
        alignItems: 'center',
        justifyContent: 'center'
    }

});

export default connect(state => state, { push })(MovieDetail)