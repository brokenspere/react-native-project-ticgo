import {createStore ,combineReducers,applyMiddleware,compose} from 'redux'
import logger from 'redux-logger'
import {routerMiddleware,connectRouter} from 'connected-react-router'
// import ProductReducer from './productReducer'
import UserReducer from '../reducers/UserReducer';

import {createMemoryHistory} from 'history'


const reducers = (history)=>combineReducers({
    user: UserReducer,
    router: connectRouter(history)
})

export const history = createMemoryHistory()

export const store = createStore(
    reducers(history),
    compose(
        applyMiddleware(
            routerMiddleware(history),
            logger
        )
    )
)
