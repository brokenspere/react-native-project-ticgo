import React , {Component} from 'react'
import {Route,Switch,Redirect} from 'react-router-native'
import {ConnectedRouter} from 'connected-react-router'
import { Provider} from 'react-redux'
import {store , history} from './AppStore'

import Register from '../containers/Register'
import Login from '../containers/Login'
import Main from '../containers/Main'
import History from '../containers/History'
import Profile from '../containers/Profile'
import MovieDetail from '../containers/MovieDetail'
import EditProfile from '../containers/EditProfile'
import BookMovie from '../containers/BookingPage'
import Summary from '../containers/Summary'

class Router extends Component{

    render(){
        return(
        <Provider store={store}>
             <ConnectedRouter history = {history}>
              <Switch>
                    <Route exact path="/" component={Login}/>
                    <Route exact path="/register" component={Register}/>
                    <Route exact path="/main" component={Main}/>
                    <Route exact path="/history" component={History}/>
                    <Route exact path="/profile" component={Profile}/>
                    <Route exact path="/detail" component={MovieDetail}/>
                    <Route exact path="/editprofile" component={EditProfile}/>
                    <Route exact path="/booking" component={BookMovie}/>
                    <Route exact path="/summary" component={Summary}/>
                    <Redirect to="/"/>
              </Switch>
          </ConnectedRouter>
        </Provider>
         
        )
    }
}
export default Router