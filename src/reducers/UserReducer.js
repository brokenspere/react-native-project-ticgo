export default (state = {}, action) => {
    switch (action.type) {
        case 'ADD_ACCOUNT':
            return {
                ...state,
                email: action.email,
                password: action.password,
                firstName: action.firstName,
                lastName: action.lastName,
                token: action.token


            }
        case 'EDIT_USER':
            return {
                ...state,
                firstName: action.firstName,
                lastName: action.lastName,
            }

        // case 'EDIT_ACCOUNT':
        // return state.map((each) => {
        //     if (each.token === action.token) {
        //         return {
        //             ...each,
        //             firstname: action.firstname,
        //             lastname: action.lastname
        //         }
        //     }
        //     return each
        // })
        case 'DELETE_USER':
            return {} 

        default:
            return state
    }
}